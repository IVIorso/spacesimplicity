﻿using UnityEngine;
using System.Collections;
//move object forward, nothing more
public class MoveForwardScript : MonoBehaviour {
	 public float movementSpeed = 3f;
	public float defaultMovespeed; //for player so that movement speed does not grow exponentially

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


		//Move the gameObject
		Vector3 pos = transform.position;

		Vector3 velocity = new Vector3 (0, movementSpeed * Time.deltaTime, 0);

		pos += transform.rotation * velocity;

		transform.position = pos;

	}


}
