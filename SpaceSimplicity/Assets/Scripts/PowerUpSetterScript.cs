﻿/*
This script sets a powerup to the player
When player enters trigger area, it will put the powerup to 
player's power up spot for usage

 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpSetterScript : MonoBehaviour {

	public PowerUpScript[] powerupList = new PowerUpScript[0];
	public PowerUpSpawner powerUpSpawner;
	public PowerupPanelScript powerUpPanel;
	SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	

	}
	//show the power up spawned
	public void ShowMe(){
		spriteRenderer.enabled = true;
	}
	// give player random powerup
	public void GivePowerUp(){
		powerUpPanel.thisPowerup = powerupList [Random.Range (0, powerupList.Length)];
		powerUpPanel.SetPowerUpStats ();

	}

	//when player collider hits spawner give the power up and fade the spawner
	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
				GivePowerUp();
				powerUpSpawner.StartCoroutine(powerUpSpawner.SpawnPowerUp());
				gameObject.transform.parent = powerUpSpawner.gameObject.transform;
				gameObject.transform.position = new Vector3(0,0,-1);
				spriteRenderer.enabled = false;
				}
	}
}
