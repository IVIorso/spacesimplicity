﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameStateMachineScript : MonoBehaviour {

	public GameObject GUIpanel;
	public GameObject gameoverPanel;
	public PlayerStatsScript playerStats;
	public GameObject bossPanel;
	public Text gameOverText;
	public GameObject mainObject;
	public Text statsText;

	//after game ends check if win or lose and setting score and win or lose message
	public void GameOver(bool isWin){
		gameoverPanel.SetActive (true);

		if (isWin) {
			gameOverText.text = "YOU WIN!";
			statsText.text = playerStats.ShowStats();

		} else {
			gameOverText.text = "YOU LOSE!";
			statsText.text = playerStats.ShowStats();
		}
		GUIpanel.SetActive (false);
		
		mainObject.SetActive (false);

	}
	//activate boss UI
	public void BossSpawned(){
		bossPanel.SetActive (true);
	}



}
