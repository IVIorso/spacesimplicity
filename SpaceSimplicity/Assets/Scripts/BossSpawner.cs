﻿using UnityEngine;
using System.Collections;

//spawn boss after certain enemyship kills, 
//grow the time on meteor spawn or disable and also enemyshipspawners

public class BossSpawner : MonoBehaviour {

	public int killcountToBoss;
	public PlayerStatsScript playerStats;
	public SpawnerScript[] meteorNenemySpawners = new SpawnerScript[0];
	public GameObject boss;
	public GameStateMachineScript gameStateMachine;

	public void CheckIfCanSpawn(){
		if (playerStats.EnemyShipKills >= killcountToBoss) {
			SpawnTheBoss();
			gameStateMachine.BossSpawned();
		}
	}

	public void SpawnTheBoss(){

		boss.transform.parent = null;
		boss.SetActive (true);
		for (int i = 0; i < meteorNenemySpawners.Length; i++) {
			meteorNenemySpawners[i].gameObject.SetActive(false);
		}

	}
}
