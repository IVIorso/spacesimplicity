﻿using UnityEngine;
using System.Collections;

public enum HealthAdjustedTarget{
	Player,
	Enemy,
	Meteor,
	Boss
}

public class DamageManagerScript : MonoBehaviour {

	public HealthAdjustedTarget targetType;


	//Does either damage or heals, only player atm can heal
	public void DoDamage(int damage){
		switch (targetType) {
		case HealthAdjustedTarget.Player:

			gameObject.GetComponent<HealthScript>().CheckTheHealth(damage, HealthAdjustmentType.TakingDamage, targetType);
			break;
		case HealthAdjustedTarget.Enemy:
		
			gameObject.GetComponent<HealthScript>().CheckTheHealth(damage, HealthAdjustmentType.TakingDamage, targetType);
			break;
		case HealthAdjustedTarget.Boss:
			gameObject.GetComponent<HealthScript>().CheckTheHealth(damage, HealthAdjustmentType.TakingDamage, targetType);
			break;
		case HealthAdjustedTarget.Meteor:
			gameObject.GetComponent<MeteorScript>().DestroyMeteorShard(damage);
			break;
		default:
			break;
		}
	}
}
