﻿using UnityEngine;
using System.Collections;
//script for immunityshield power up 
public class ImmunityScript : MonoBehaviour {

	PlayerStatsScript playerStats ;
	LayerMask playerLayer;
	void Start () {
	
		playerLayer = gameObject.layer;
		playerStats  = gameObject.GetComponentInParent<PlayerStatsScript> ();

	}

	public IEnumerator StartShield(){
		//after 6 seconds will fade the shield
		yield return new WaitForSeconds (6f);
		gameObject.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D col){
		//destroy those objects that can be destroyed by immunityshield and add points
		if (col.gameObject.layer != playerLayer) {
		
			if (col.tag == "Meteor") {
				playerStats .AddPoints(col.GetComponent<MeteorScript>().points);
				Destroy(col.gameObject);
			}

			else if (col.tag == "Enemy") {
				
				playerStats .EnemyKilled();
				Destroy(col.gameObject);
			}



			if(col.tag == "Boss"){
					col.GetComponent<DamageManagerScript>().DoDamage(-1);
			}

				
		}
	}
}

