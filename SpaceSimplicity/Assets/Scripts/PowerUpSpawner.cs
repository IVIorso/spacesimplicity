﻿using UnityEngine;
using System.Collections;
/*
 * This script spawns different powerups, which are in the PowerupList,
 * Powerups spawn in specific time, like every 20 sec, for first every 5 sec, to test all work
 * After powerup is picked up the countdown will start
 * Powerups now(22.11.2015) are: HealthPackage(restore 3 health), Immunityshield(destroys meteors and is
 * immune to damage)
 * Player decides when use the powerup
 * In future: MassiveBullets (increase size and make bullets to one-hit)
 */
using System.Collections.Generic;


public class PowerUpSpawner : MonoBehaviour {


	public float spawnTime;
	public PowerUpSetterScript powerUpSetter;
	GameObject powerUp;


	// Use this for initialization
	void Start () {
		StartCoroutine(	SpawnPowerUp());
		powerUpSetter = transform.GetChild (0).GetComponent<PowerUpSetterScript> ();
		powerUp = powerUpSetter.gameObject;

	}
	//spawn power up
	public IEnumerator SpawnPowerUp(){
		yield return new WaitForSeconds(spawnTime);
		powerUpSetter.ShowMe ();
		powerUp.transform.parent = null;
		powerUp.SetActive (true);
	

	}
}
