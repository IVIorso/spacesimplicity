﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossBehaviourScript : MonoBehaviour {

	public Transform player;
	public float rotSpeed = 90;
	public float maxDistance;
	public ShootingScript[] shootSc = new ShootingScript[0];
	public int points;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (player == null) {
			//let's find the player ship
			GameObject go = GameObject.Find ("Player");
			
			if(go != null){
				player = go.transform;
			}
			
		}
		
		//player found, kill it! 
		
		//or dead already?
		if (player == null)
			return;
		
		//found, kill!!!!!
		
		
		Vector3 dir = player.position - transform.position;
		float dis = Vector3.Distance (player.position, transform.position);
		dir.Normalize ();
		
		float zAng = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;
		
		Quaternion rot_z = Quaternion.Euler (0f, 0f, zAng);
		
		transform.rotation = Quaternion.RotateTowards (transform.rotation, rot_z, rotSpeed * Time.deltaTime);
		//dis < maxDistance
		if ( dis < maxDistance) {
			for(int i = 0; i < shootSc.Length; i++){
				shootSc[i].Shoot ();
			}

			
		}
	}

}
