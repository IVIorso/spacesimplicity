﻿using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour {

	public GameObject prefab;
	public float meteorSpeed;
	public Transform[] directions = new Transform[0];
	public float spawnTime;
	public int spawningAmountMax, spawningAmountCur; // amount that one spawner can spawn, will be checked so that there wont be millions of objects.
	public bool isMeteor;
	public PlayerStatsScript playerStats;


	// Use this for initialization
	void Start () {
		StartCoroutine (SpawnObject());
	}


	//used for so that can spawn after spawned enemy has been killed
	public void BabyDied(int p){
		spawningAmountCur --;
		playerStats.EnemyKilled();
		playerStats.AddPoints (p);

		StartCoroutine (SpawnObject());
	}
	//spawning object if has not spawned more object than max and they has not been killed
	//works differently when spawning meteor or enemy
	public IEnumerator SpawnObject(){
		while (spawningAmountCur >= spawningAmountMax) {
			GameObject go = (GameObject)Instantiate (prefab);
			go.transform.position = gameObject.transform.position;
			gameObject.transform.parent = gameObject.transform;
			if(isMeteor){
				go.GetComponent<MeteorScript>().spawner = gameObject.GetComponent<SpawnerScript>();
			}
			else if(!isMeteor){
				go.GetComponent<EnemyBehaviour>().spawner = GetComponent<SpawnerScript>();
			}
			Vector3 diff = directions[Random.Range(0, directions.Length)].position - transform.position;
			diff.Normalize();
			go.GetComponent<Rigidbody2D>().AddForce(diff * meteorSpeed, ForceMode2D.Force);
			spawningAmountCur ++;
			yield return new WaitForSeconds(spawnTime);
		}

		StopCoroutine(SpawnObject());



	}
}
