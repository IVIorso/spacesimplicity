﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {


	public int damage;
	public string[] tagsToHit;//colletion of tags that bullet can hit

	// Use this for initialization
	void Start () {
		StartCoroutine (WaitToDie ());
	}

	void OnTriggerEnter2D(Collider2D col){
		for (int i = 0; i < tagsToHit.Length; i++) {

			if(col.tag == tagsToHit[i]){

				if(col.tag == "Player" || col.tag == "Enemy" || col.tag == "Boss"){

					col.gameObject.GetComponentInChildren<DamageManagerScript>().DoDamage(-damage);

				}

				else if (col.tag == "Meteor"){
					col.gameObject.GetComponent<DamageManagerScript>().DoDamage(-damage);
				}
				
				Destroy(gameObject);
			}
		}

	}
	
	public IEnumerator WaitToDie(){
		//bullet dies after 2 seconds
		yield return new WaitForSeconds(2f);
		Destroy (gameObject);
	}
}
