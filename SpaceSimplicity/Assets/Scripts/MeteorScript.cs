﻿using UnityEngine;
using System.Collections;
//meteor behaviour script
public class MeteorScript : MonoBehaviour {

	public int health;
	public int points;
	public bool isShard= false;
	public GameObject[] shards;
	public float speed;
	public PlayerStatsScript playerStats;
	public GameObject parent;
	private SpriteRenderer spiteRenderer;
	public SpawnerScript spawner;

	public float waitToDieTime = 5f;

	// Use this for initialization
	void Start () {

		StartCoroutine (DestroyWhenTimeOut());
		playerStats = spawner.playerStats;
		spiteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//destroy after time, so that game won't have too many objects
	IEnumerator DestroyWhenTimeOut(){
		yield return new WaitForSeconds (waitToDieTime);
		Destroy (gameObject);
	
	}

	// Creates the shards. and sends them forward. 

	public void CreateShards(){
		for (int i = 0; i < shards.Length; i++) {
			shards[i].SetActive(true);
			parent = shards[i].transform.parent.gameObject;

			shards[i].GetComponent<MeteorScript>().playerStats = playerStats;
			playerStats.AddPoints(points);
			shards[i].transform.parent = null;

			parent.SetActive(false);
		
			Vector3 vect = new Vector3(-i,i-1, 0);
			vect.Normalize();
			shards[i].GetComponent<Rigidbody2D>().AddForce(vect * speed);
		}

	}

	// Destroies the meteor shard.
	// If shard, shard takes damage, if not this object is meteor and takes damage
	// also if meteor and dies, meteor kills ++ and create shards
	// <param name="damage">Damage.</param>
	public void DestroyMeteorShard(int damage){
		if (isShard) {
			health -= damage;
			if(health <= 0){
				Destroy(gameObject);

				if(parent != null){
					Destroy(parent);
				}
			}

		
		} 
	
		else if (!isShard) {
			health --;
			spiteRenderer.color = Color.Lerp(Color.green, Color.red, 1f);
			if(health <= 0){
				GameObject.Find("Player").GetComponent<PlayerStatsScript>().meteorKills++;
				CreateShards();
				StopCoroutine(DestroyWhenTimeOut());
			}
	

		}
	}

}
