﻿using UnityEngine;
using System.Collections;
//this script follows the player
public class followPlayer : MonoBehaviour {
	public Transform playerPos;
	public float myZ;

	// Update is called once per frame
	void Update () {

		if (playerPos.gameObject != null) {
			gameObject.transform.position = new Vector3 (playerPos.position.x, playerPos.position.y, myZ);
		}
		}

}
