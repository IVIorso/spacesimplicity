﻿using UnityEngine;
using System.Collections;
//load level set at Unity editor's inspector
public class LoadLevelScript : MonoBehaviour {
	public int loadLevelID;


	public void LoadLevel(){
		Application.LoadLevel (loadLevelID);

	}

	public void ExitGame(){
		Application.Quit ();
	}
}
