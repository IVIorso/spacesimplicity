﻿using UnityEngine;
using System.Collections;

public enum ActionType{
	healing,
	powerupDamage,
	immunity
}
//this is power up script
public class PowerUpScript : MonoBehaviour {

	public string name;
	public Sprite icon;
	public ActionType actionType;


	// Use this for initialization
	void Start () {
	
	}
	//power up's usage
	public void Action(PlayerManagerScript plManager){
		switch (actionType) {
			//healing powerup
		case ActionType.healing:
			GetComponent<healthPackageScript>().Heal(plManager);
			break;
			//immunity shield powerup
		case ActionType.immunity:

			plManager.immunityShield.SetActive(true);
			plManager.immunityShield.GetComponent<ImmunityScript>().StartCoroutine(plManager.immunityShield.GetComponent<ImmunityScript>().StartShield());
			break;
		case ActionType.powerupDamage:
			//this has not been done. maybe coming in the future
			break;
		default:
			break;
		}

	}
}
