﻿using UnityEngine;
using System.Collections;
//move background plane with player and "Move" offset so that feels that space is moving around you
public class MoveBackgroundScript : MonoBehaviour {

	MeshRenderer meshRenderer;
	Material material;
	Vector2 offset;
	public GameObject player;

	void Start(){
		meshRenderer = GetComponent<MeshRenderer> ();
		material = meshRenderer.material;
		offset = material.mainTextureOffset;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, 10);
		offset.x = transform.position.x / (transform.localScale.x*10);
		offset.y = transform.position.y / (transform.localScale.y*10);
		material.mainTextureOffset = -offset;
	}
}
