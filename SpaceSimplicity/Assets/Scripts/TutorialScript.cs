﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum TutorialPhase{
	Left,
	Right,
	Forward,
	Backward,
	Shoot
}
//tutorial to help player to understand games movement
public class TutorialScript : MonoBehaviour {

	public bool isRotateLeft;
	public bool isForward;
	public bool isShooting;
	public TutorialPhase tutorialPhase;
	public int tutorialPhaseIndex;
	public GameObject spawner;
	public Text tutorialText;

	// Use this for initialization
	void Start () {
		tutorialText.text = "Welcome, here is the tutorial. "+ "\n"+"First Tilt phone's left side to rotate left";
		tutorialPhaseIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//tutorial texts, pictures would be nice too
	public void Tutorial(){
		switch (tutorialPhaseIndex) {
		case 1:

			tutorialText.text = "Now TILT phone's right side to rotate right";
			break;
		case 2:
			tutorialText.text = "Now TILT phone's forward to add speed";
			break;
		case 3:
			tutorialText.text = "Now TILT phone's backward to slow speed";
			break;
		case 4:
			tutorialText.text = "Press the screen to shoot";
			break;
		case 5:
			tutorialText.text = "Well done! tutorial complete";
			spawner.SetActive(true);
			gameObject.SetActive(false);
			break;
		default:
			break;
		}
	}
	//what phase is on
	public void Phase(){
		if ( tutorialPhase == TutorialPhase.Left  && tutorialPhaseIndex == 0) {
			tutorialPhaseIndex ++;
			Tutorial();
		}

		if ( tutorialPhase == TutorialPhase.Right && tutorialPhaseIndex == 1) {
			tutorialPhaseIndex ++;
			Tutorial();
		}

		if ( tutorialPhase == TutorialPhase.Forward  && tutorialPhaseIndex == 2)  {
			tutorialPhaseIndex ++;
			Tutorial();
		}

		if ( tutorialPhase == TutorialPhase.Backward && tutorialPhaseIndex == 3) {
			tutorialPhaseIndex ++;
			Tutorial();
		}

		if ( tutorialPhase == TutorialPhase.Shoot && tutorialPhaseIndex == 4) {
			tutorialPhaseIndex++;
			Tutorial();
		}
	}


}
