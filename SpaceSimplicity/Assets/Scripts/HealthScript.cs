using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum HealthAdjustmentType{
	TakingDamage,
	Healing
}


public class HealthScript : MonoBehaviour {


	public List<HeartScript> health =  new List<HeartScript>();
	public float currentHealth;
	public float maxHealth;
	public PlayerMovementScript pla;
	public DamageManagerScript dmgManager;
	private HealthAdjustedTarget target;
	public Slider bossHealth;
	public EnemyBehaviour myBeh;


	//checking hearths and setting images
	//taking last index with full heart and then
	// putting all empty, then refil those hearts that should be
	//to show how much hp left
	public void CheckHearts(){

		int x = (int)currentHealth / 2;
		int over = (int)(currentHealth - x*2);
		int lastIndex = 0;
		
		
		for(int i =0; i < (maxHealth/2); i++){
			health [i].SetHeartImage (0);
			
			
		}
		
		for(int i =0; i < x; i++){
			health [i].SetHeartImage (2);
			lastIndex = i;
			
		}
		
		
		if(over > 0){
			health [lastIndex+1].SetHeartImage (over);
		}
		

	}

	public void CheckTheHealth(int adj, HealthAdjustmentType haType, HealthAdjustedTarget targ){
		target = targ;
		currentHealth += adj;
		if (currentHealth > 0) {
			switch (haType) {
			case HealthAdjustmentType.Healing:

				if (currentHealth > 0) {
					if(currentHealth > maxHealth){
						currentHealth = maxHealth;
					}	
				}
				break;
			case HealthAdjustmentType.TakingDamage:
			
				if(pla != null){
					pla.animator.SetBool("takingDamage", true);
				}

				break;
			default:
				
				break;
			}
			if(targ == HealthAdjustedTarget.Player){
				CheckHearts();
			}
			if(targ == HealthAdjustedTarget.Boss){
				bossHealth.value = currentHealth;
			}

		} else {
			if(targ == HealthAdjustedTarget.Player){
				//Player dead

				GameObject.Find("Canvas").GetComponent<GameStateMachineScript>().GameOver(false);
				Destroy(pla.gameObject);
			}
			else if(targ == HealthAdjustedTarget.Enemy){
				//enemy dead, inform spawner and destroy this enemy

				myBeh.spawner.BabyDied(myBeh.points);
			
				Destroy(gameObject.transform.parent.gameObject);
			}
			else if(targ == HealthAdjustedTarget.Boss ){
				//boss destroyed inform gamestatemachine, win
				
				GameObject.Find("Canvas").GetComponent<GameStateMachineScript>().GameOver(true);
				
				Destroy(gameObject.transform.parent.gameObject);
			}



		}


	}
	void OnTriggerEnter2D(Collider2D col){
		//if object that can harm you hits you, lose 1 damage
		if (target == HealthAdjustedTarget.Player) {
			if (col.tag == "Meteor" || col.tag == "Enemy" || col.tag == "Boss") {
				dmgManager.DoDamage(-1);
				
			}
		}
		if (target == HealthAdjustedTarget.Enemy) {
			if (col.tag == "Meteor" || col.tag == "Player") {
				dmgManager.DoDamage(-1);
				
			}
		}
		if (target == HealthAdjustedTarget.Boss) {
			if (col.tag == "Meteor" || col.tag == "Player") {
				dmgManager.DoDamage(-1);
				
			}
		}
	}

}
