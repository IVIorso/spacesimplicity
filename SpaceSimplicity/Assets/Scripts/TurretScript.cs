﻿using UnityEngine;
using System.Collections;
//script for boss turret, rotates turret and shoots
public class TurretScript : MonoBehaviour {

	public GameObject turret;
	public float minDegree, maxDegree;
	public bool isRotatingLeft;
	public float speed;

	private float rotationPoint1 = 0.7f;
	private float rotationPoint2 = 0.9f;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		RotateMe ();
		
	}


	public void RotateMe(){
		float newZposition = turret.transform.rotation.z * Time.deltaTime * speed;

		if (turret.transform.rotation.z <= rotationPoint1 && isRotatingLeft ) {
			speed = -speed;
			isRotatingLeft = false;
		} 
		if (turret.transform.rotation.z >= rotationPoint2 && !isRotatingLeft ) {
			isRotatingLeft = true;
		}

			turret.transform.Rotate (new Vector3 (0, 0, newZposition));


		
	}


}
