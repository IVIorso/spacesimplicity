﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {

	public Transform playerTransform;
	public float rotationSpeed;
	public float maxDistance;
	public ShootingScript[] shootingScripts = new ShootingScript[0];
	public SpawnerScript spawner;
	public int points;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (playerTransform == null) {
			//let's find the player ship
			GameObject go = GameObject.Find ("Player");

			if(go != null){
				playerTransform = go.transform;
			}



		
		}
		

		//player found, kill it! 


		//taking direction and distance. then normalize direction
		Vector3 dir = playerTransform.position - transform.position;
		float dis = Vector3.Distance (playerTransform.position, transform.position);
		dir.Normalize ();

		//start rotationing
		float zAng = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;
		
		Quaternion rot_z = Quaternion.Euler (0f, 0f, zAng);
		
		transform.rotation = Quaternion.RotateTowards (transform.rotation, rot_z, rotationSpeed * Time.deltaTime);

		//will only shoot if player is closer than maxdistance
		if (dis < maxDistance) {
			for(int i = 0; i <shootingScripts.Length; i++){
				shootingScripts[i].Shoot ();
			}
		

		}

	}


}
