﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovementScript : MonoBehaviour {

	private Rigidbody2D myBody;
	private MoveForwardScript moveForwardScript;
	private const float  ROTATE_AMOUNT = 1f; // at full tilt, rotate at 1 degrees per update
	private Vector3 directionOfTouch; //used on touch movement, not using atm
	private float tiltYScaleMax = 1f; //used to prevent ship going backwards.
	private const float TILT_MIN = 0.01f;
	private const float TILT_MAX = 0.1f;
	public float speed;
	public ShootingScript shootScript;
	public bool dead;
	public float rotationSpeed;
	public TutorialScript tutorialScript;
	public Animator animator;

	//tilting x angle and rotate with this value
	float GetTiltXValue() {
		const float TILT_MIN = 0.05f;
		const float TILT_MAX = 0.2f;
		
		// how much the tilt has effect
		float tilt = Mathf.Abs(Input.acceleration.x);
		
		// if not tilted over min value dont do anything
		if (tilt < TILT_MIN) {
			return 0;
		}
		float tiltScale = (tilt - TILT_MIN) / (TILT_MAX - TILT_MIN);

		// scale to negative if negative angle
		if (Input.acceleration.x < 0) {
			return -tiltScale;
		} else {
			return tiltScale;
		}
		
	}

	float GetTiltYValue() {
		const float TILT_MIN = 0.01f;
		const float TILT_MAX = 0.1f;	
		// how much the tilt has effect
		float tilt = Mathf.Abs(Input.acceleration.y);
		
		// if not tilted over min vlue dont do anything
		if (tilt < TILT_MIN) {
			return 0;
		}
		float tiltScale = (tilt - TILT_MIN) / (TILT_MAX - TILT_MIN);


		if (tiltScale > tiltYScaleMax) {
			tiltScale = tiltYScaleMax; //preventing for ship to go backwards.
		}

	
		// scale to negative if negative angle
		if (Input.acceleration.y < 0) {
			return -tiltScale;
		} else {
			return tiltScale;
		}
		
	}
	// Use this for initialization
	void Start () {
		myBody = gameObject.GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		moveForwardScript = GetComponent<MoveForwardScript> ();
		if(!Input.gyro.enabled)
		{
			Input.gyro.enabled = true;
		}

	}




	// Update is called once per frame
	void Update () {
		if (!dead) {
			animator.SetInteger ("MotorPhase", 1);
			//as player tilts phone the movement is done
			float tiltXValue = GetTiltXValue (); // tilting sides will make the player to rotate
			//tutorial check
			if(tiltXValue > 0){
				tutorialScript.tutorialPhase = TutorialPhase.Left;
				tutorialScript.Phase();
			}
			//tutorial check
			if(tiltXValue < 0){
				tutorialScript.tutorialPhase = TutorialPhase.Right;
				tutorialScript.Phase();
			}

			float tiltYValue = GetTiltYValue ();//tilting forwards or backwards makes the player go faster or slower
			//tutorial check
			if(tiltYValue > 0){
				tutorialScript.tutorialPhase = TutorialPhase.Forward;
				tutorialScript.Phase();
			}
			//tutorial check
			if(tiltYValue < 0){
				tutorialScript.tutorialPhase = TutorialPhase.Backward;
				tutorialScript.Phase();
			}
			
			//take old angles to count
			Vector3 oldAngles = this.transform.eulerAngles;
			//rotate player
			this.transform.eulerAngles = new Vector3 (oldAngles.x, oldAngles.y, oldAngles.z + (tiltXValue * ROTATE_AMOUNT));
			//add speed
			moveForwardScript.movementSpeed = moveForwardScript.defaultMovespeed + tiltYValue;
			//shoot on touch
			if (Input.touches.Length > 0) {
				tutorialScript.tutorialPhase = TutorialPhase.Shoot;
				//tutorial check

				tutorialScript.Phase();
				shootScript.Shoot ();
			}
			/* here is the other way to do the moving, touch one plays and the player follows
			 * I wanted to do with tilting because it was more interesting way to do it
			 * 
				 * 
				 * 		anim.SetInteger ("MotorPhase", 1);
	if (Input.touches [0].phase == TouchPhase.Began) {
					//start the engine

					
				}
				if (Input.touches [0].phase == TouchPhase.Moved) {
					//Rotate towards the finger, lose little of the speed

					anim.SetInteger ("MotorPhase", 2);

					directionOfTouch = Camera.main.ScreenToWorldPoint (Input.touches [0].position) - transform.position;
					directionOfTouch.Normalize ();
					
					float rot_z = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
					transform.rotation = Quaternion.Euler (0f, 0f, rot_z - 90);



					
				}
				
				if (Input.touches [0].phase == TouchPhase.Stationary) {
					//move towards the finger

					directionOfTouch = Camera.main.ScreenToWorldPoint (Input.touches [0].position) - transform.position;
					directionOfTouch.Normalize ();
					myBody.AddForce (directionOfTouch * speed);


				}
				
				if (Input.touches [1].phase == TouchPhase.Began || Input.touches [1].phase == TouchPhase.Stationary) {
					//start the engine

				}
				                                */

		
				
				
			} 
		}

	//used for taking damage. ending it

	public void AnimationEnded(){
		animator.SetBool ("takingDamage", false);

	}
}
