﻿using UnityEngine;
using System.Collections;

public class healthPackageScript : MonoBehaviour {
	//this script only for health package power up
	private HealthScript playerHealth;
	public int healthRecover;


	//healing player (player can only heal)
	public void Heal(PlayerManagerScript plManager){
		playerHealth = plManager.playerHealth;
		playerHealth.CheckTheHealth(healthRecover, HealthAdjustmentType.Healing, HealthAdjustedTarget.Player);

	}
}
