﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//use this to PowerUp UI behaviour
public class PowerupPanelScript : MonoBehaviour {

	public Text powerupName;
	public Image powerupImage;
	public PowerUpScript thisPowerup;
	public GameObject powerUpButton;
	public PlayerManagerScript playerManager;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//set the power up, now use able
	public void SetPowerUpStats(){

		powerupName.text = thisPowerup.name;
		powerupImage.sprite = thisPowerup.icon;
		powerUpButton.SetActive (true);
	}
//use power up
	public void UsePowerUp(){
		thisPowerup.Action (playerManager);
		powerupName.text = "";
		powerUpButton.SetActive (false);


	}
}
