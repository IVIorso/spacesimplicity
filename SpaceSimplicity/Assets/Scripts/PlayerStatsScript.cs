﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//player status. kills, point texts, etc
public class PlayerStatsScript : MonoBehaviour {

	public int meteorKills;
	public int EnemyShipKills;
	public Text enemykillstext;
	public Text meteorKillstext;
	public Text pointsText;
	public BossSpawner bossSpawner;
	public int points;


	//enemy killed, then check if boss can be spawned
	public void EnemyKilled(){
		EnemyShipKills++;
		enemykillstext.text = "Kills: " + EnemyShipKills + " / " + bossSpawner.killcountToBoss;
		bossSpawner.CheckIfCanSpawn ();
	}
	//meteor kills add
	public void MeteorKilled(){
		meteorKills ++;

		
	}
//show score
	public string ShowStats(){
		string st = "Points: " + points;
		return st;
	}
	// add score
	public void AddPoints(int p){
		points += p;
		pointsText.text = "Points: " + points;
		
	}
		





}
