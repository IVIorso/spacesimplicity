﻿using UnityEngine;
using System.Collections;
//shooting script works on player and enemies
public class ShootingScript : MonoBehaviour {

	public GameObject prefab;

	public float bulletSpeed;
	public Transform direction;
	public float overheatCurrent;
	public float overheatMax;
	public float cooldown;
	public bool isOverheating;
	public float repeatTime = 0.5f;
	float timer;

	void Update(){
		if (timer > 0) {
			timer -= Time.deltaTime;
		}

	}

	//shoot, has over heating (on player 1000 for not over heating, have not implemented ui for this)
	//creates bullets out of prefab, repeat time is used for not shooting 1 million bullets in second,
	//also repeat time adjusting will make enemies easier, when they shoot at the moment 1 bullet per second
	public void Shoot(){
		if (overheatCurrent >= overheatMax) {
			isOverheating = true;
		}
		if (overheatCurrent < overheatMax) {
			isOverheating = false;
		}

		if (!isOverheating && timer <= 0) {
			GameObject go = (GameObject)Instantiate (prefab, direction.position, direction.rotation);
			overheatCurrent ++;
			timer += repeatTime;
			StopAllCoroutines();
			StartCoroutine(CoolDown());
		} else if(isOverheating){
			isOverheating = true;
			StartCoroutine(CoolDown());
		}
	}
	//cooldown if overheated
	public IEnumerator CoolDown(){
		yield return new WaitForSeconds (1f);
		if (isOverheating) {
			yield return new WaitForSeconds (cooldown);
			overheatCurrent = 0;
		} else {
			while(overheatCurrent >0){
				yield return new WaitForSeconds (0.05f);
				overheatCurrent --;
			}

		}
	
	}

}
