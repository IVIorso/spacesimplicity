# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository is for my portfolio game SpaceSimplicity
Game is created using Unity and scripted with C#. Graphics are my own design using Gimp and MS paint. This game also has a few animations that have been done with Unity's own animator.

### How do I get set up? ###


You can check source code from sources in here at Bitbucket. Here is also a few links to get started up if you want to get this source code and run it on your on PC

Get Git here: https://git-scm.com/downloads
Locate on git terminal to where you want to go (for example: cd documents -> new folder)
clone repository using my ssh link : git clone git@bitbucket.org:IVIorso/spacesimplicity.git

now you have the repository!
for using you also need unity: https://unity3d.com/get-unity

To play the game you must have Android and install the .apk.